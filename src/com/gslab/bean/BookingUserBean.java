package com.gslab.bean;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BookingUserBean {
	
	private String isLunchSlotBooked;
	private String isSnacksSlotBooked;
	
	private Calendar date;
	
	@Autowired
	@Qualifier("loginBean")
	private LoginBean loginBean;

	public String getIsLunchSlotBooked() {
		return isLunchSlotBooked;
	}

	public void setIsLunchSlotBooked(String isLunchSlotBooked) {
		this.isLunchSlotBooked = isLunchSlotBooked;
	}

	public String getIsSnacksSlotBooked() {
		return isSnacksSlotBooked;
	}

	public void setIsSnacksSlotBooked(String isSnacksSlotBooked) {
		this.isSnacksSlotBooked = isSnacksSlotBooked;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}

	@Override
	public String toString() {
		return "BookingUserBean [isLunchSlotBooked=" + isLunchSlotBooked
				+ ", isSnacksSlotBooked=" + isSnacksSlotBooked + ", date="
				+ date + ", loginBean=" + loginBean + "]";
	}
	
	

}
