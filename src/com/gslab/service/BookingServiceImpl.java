package com.gslab.service;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.gslab.bean.BookingBean;
import com.gslab.bean.SongsBean;
import com.gslab.dao.BookingDaoI;
import com.gslab.dao.LoginDaoI;

@Service("BookingService")
public class BookingServiceImpl implements BookingServiceI {

	@Autowired
	@Qualifier("bookingDao")
	private BookingDaoI bookingDaoImpl;
	
	/* (non-Javadoc)
	 * @see com.gslab.service.BookingServiceI#getAfternoonSessionSongs(java.sql.Date)
	 */
	public  List<BookingBean> getAfternoonSessionSongs(Date date) {
		return bookingDaoImpl.getAfterNoonSessionList(date);
	}
	
	/* (non-Javadoc)
	 * @see com.gslab.service.BookingServiceI#getEveningSessionSongs(java.sql.Date)
	 */
	public List<BookingBean> getEveningSessionSongs(Date date) {
		return bookingDaoImpl.getEveningSessionList(date);
	}
	
	public java.util.Date getToday() {
		Calendar calendar = Calendar.getInstance();
		return calendar.getTime();
	}
	
	public boolean bookSongSlot(String username,int slotNo,int sessionId,int songId) {
		java.util.Date currentDate = this.getToday();
		java.sql.Date date = new java.sql.Date(currentDate.getTime());		
		return bookingDaoImpl.bookSongSlot(username, slotNo, sessionId, songId, date);
	}
	
	public List<SongsBean> searchSong(String query) {
		return bookingDaoImpl.search(query);
	}
	
	public List<SongsBean> getSongs(){
		return bookingDaoImpl.getAllSongs();
	}

	public boolean getSlotStatus(int slot, int sessionId, Date date) {
		// TODO Auto-generated method stub
		return bookingDaoImpl.getSlotStatus(slot, sessionId, date);
	}
}
