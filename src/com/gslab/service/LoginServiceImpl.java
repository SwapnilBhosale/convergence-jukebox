package com.gslab.service;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.gslab.bean.BookingUserBean;
import com.gslab.bean.LoginBean;
import com.gslab.dao.LoginDaoI;

@Service("LoginService")
public class LoginServiceImpl implements LoginServiceI {

	@Autowired
	@Qualifier("loginDao")
	private LoginDaoI loginDaoImpl;
	
	/* (non-Javadoc)
	 * @see com.gslab.service.LoginServiceI#checkUser(java.lang.String, java.lang.String)
	 */
	public LoginBean checkUser(String username,String password) {
		return loginDaoImpl.validateUser(username, password);
	}

	public BookingUserBean getBookingStatus(String userName, Date date) {
		// TODO Auto-generated method stub
		return loginDaoImpl.getBookingStatus(userName, date);
	}

	public boolean updateUserBookings(String username, int sessionId,
			Date currentDate) {
		// TODO Auto-generated method stub
		return loginDaoImpl.updateUserBookings(username, sessionId, currentDate);
	}
}
