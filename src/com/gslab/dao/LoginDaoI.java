package com.gslab.dao;

import java.sql.Date;

import com.gslab.bean.BookingUserBean;
import com.gslab.bean.LoginBean;

public interface LoginDaoI {

	public abstract LoginBean validateUser(String userName, String password);
	
	public abstract BookingUserBean getBookingStatus(String userName,Date date);
	public abstract boolean updateUserBookings(String username, int sessionId,Date currentDate);

}