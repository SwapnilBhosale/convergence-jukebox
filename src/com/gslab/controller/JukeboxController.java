package com.gslab.controller;

import java.awt.PageAttributes.MediaType;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gslab.bean.BookingBean;
import com.gslab.bean.BookingUserBean;
import com.gslab.bean.LoginBean;
import com.gslab.bean.SongsBean;
import com.gslab.service.BookingServiceI;
import com.gslab.service.LoginServiceI;

@Controller
public class JukeboxController {

	LoginBean loginBean = null;

	@Autowired
	@Qualifier("LoginService")
	private LoginServiceI loginService;

	@Autowired
	@Qualifier("BookingService")
	private BookingServiceI bookingService;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@RequestMapping("/index")
	public String showIndex(Model model) {
		model.addAttribute("loginBean", new LoginBean());
		return "Login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView validateUser(
			@Valid @ModelAttribute("loginBean") LoginBean bean,
			BindingResult bs, Model model, final HttpServletRequest request,
			HttpSession session) throws Exception {
		if (bs.hasErrors()) {
			model.addAttribute("loginBean", new LoginBean());
			return new ModelAndView("Login");
		} else {
			try {
				loginBean = loginService.checkUser(bean.getUsername(),
						bean.getPassword());
				session.invalidate();
				System.out.println("Login Bean : " + loginBean);
				HttpSession newSession = request.getSession(true);
				newSession.setAttribute("userInfo", loginBean);
				java.util.Date currentDate = bookingService.getToday();
				java.sql.Date date = new java.sql.Date(currentDate.getTime());
				System.out.println("Sql Date" + date);
				System.out.println("java date : " + currentDate);

				List<BookingBean> list1 = bookingService
						.getAfternoonSessionSongs(date);
				List<BookingBean> list2 = bookingService
						.getEveningSessionSongs(date);
				List<SongsBean> list3 = bookingService.getSongs();
				Collections.sort(list1);
				Collections.sort(list2);

				System.out.println(list1.toString());
				System.out.println(list2.toString());
				// model.addAttribute("userInfo", loginBean);

				BookingUserBean bookingStatusBean = loginService
						.getBookingStatus(bean.getUsername(), date);

				System.out.println("Booking Status Controller : "
						+ bookingStatusBean);

				model.addAttribute("lunchSongsList", list1);
				model.addAttribute("snacksSongsList", list2);
				model.addAttribute("bookingStatus", bookingStatusBean);
				System.out.println("Songs List : " + list3.toString());
				model.addAttribute("songList", list3);
				return new ModelAndView("home");
			} catch (EmptyResultDataAccessException e) {
				model.addAttribute("error",
						"Please Enter a Valid Credentials...!!!");
				model.addAttribute("loginBean", new LoginBean());
				return new ModelAndView("Login");
			} catch (Exception e) {
				throw e;
			}
		}
	}

	@RequestMapping(value = "/book_slot", method = RequestMethod.POST)
	public ModelAndView bookSlot(Model model,
			@RequestParam("Sel_slot") int slotNo,
			@RequestParam("Sel_session") int sessionId,
			@RequestParam("Sel_song") int songId, HttpSession session) {

		java.util.Date currentDate = bookingService.getToday();
		java.sql.Date date = new java.sql.Date(currentDate.getTime());
		LoginBean userInfo = (LoginBean) session.getAttribute("userInfo");
		System.out.println("Booked By : " + userInfo.getUsername());

		Boolean isSlotBooked = bookingService.getSlotStatus(slotNo, sessionId,
				date);
		if (isSlotBooked) {
			model.addAttribute("errorName", "Slot already booked..!!");

		} else {
			bookingService.bookSongSlot(userInfo.getUsername(), slotNo,
					sessionId, songId);
			loginService.updateUserBookings(userInfo.getUsername(), sessionId,
					date);
		}
		List<BookingBean> list1 = bookingService.getAfternoonSessionSongs(date);
		List<BookingBean> list2 = bookingService.getEveningSessionSongs(date);
		List<SongsBean> list3 = bookingService.getSongs();
		// LoginBean bean = new LoginBean();
		// bean.setUsername(username);
		model.addAttribute("songList", list3);
		model.addAttribute("lunchSongsList", list1);
		model.addAttribute("snacksSongsList", list2);
		// model.addAttribute("userInfo", bean);
		return new ModelAndView("home");
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET, headers = "Accept=*/*")
	public @ResponseBody List<SongsBean> getSongs(
			@RequestParam("q") String query) {
		System.out.println(bookingService.searchSong(query));
		return bookingService.searchSong(query);
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(Model model, final HttpServletRequest request,
			HttpSession session) {
		session.invalidate();
		model.addAttribute("loginBean", new LoginBean());
		return new ModelAndView("Login");
	}
}
