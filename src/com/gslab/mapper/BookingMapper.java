package com.gslab.mapper;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import org.springframework.jdbc.core.RowMapper;

import com.gslab.bean.BookingBean;
import com.gslab.bean.LoginBean;
import com.gslab.bean.SongsBean;
import com.gslab.bean.BookingBean;

public class BookingMapper implements RowMapper<BookingBean> {

	public BookingBean mapRow(ResultSet rs, int arg1) throws SQLException {
		BookingBean bean = new BookingBean();
		LoginBean lBean= new LoginBean();
		lBean.setUsername(rs.getString(1));
		bean.setLoginBean(lBean);
		bean.setSlotNo(rs.getInt(2));
		bean.setSessionId(rs.getInt(3));
		bean.setBooked(rs.getInt(4));
		String slotName = "";
		if(bean.getSessionId() == 1 ){
			slotName = BookingBean.lunchSlotName[bean.getSlotNo()];
		} else {
			slotName = BookingBean.snacksSlotName[bean.getSlotNo()];
		}
		bean.setSlotName(slotName);
		Date date = rs.getDate(5);
		long time = date.getTime();
		java.util.Date newDate = new java.util.Date(time);
		Calendar cal = Calendar.getInstance();
		cal.setTime(newDate);
		bean.setDate(cal);
		
		SongsBean bean1 = new SongsBean();
		if(rs.getInt(6) == 0) {
			bean1.setSongId(rs.getInt(6));
			bean1.setName(null);
			bean1.setPath(null);
		} else {		
			bean1.setSongId(rs.getInt(6));
			bean1.setName(rs.getString(7));
			bean1.setPath(rs.getString(8));
		}
		bean.setSongBean(bean1);
		return bean;
	}

}
