<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/Login.css" rel="stylesheet">
    <title>Login to Jukebox</title>
    </head>
    <body>
    <div class="container">
        <form:form class="form-signin" method="post" action="login.html" modelAttribute="loginBean">
            <h2 class="form-signin-heading">Login Here</h2>
            <c:if test="${error ne null or error eq ''}">
				<font color="red">${error}<br /></font>
			</c:if>
        	<form:input class="input-block-level" path="username" placeholder="Enter Username"/><form:errors path="username"/>
        	<form:password class="input-block-level" path="password" placeholder="Enter password"/><form:errors path="password"/>
	        	<button class="btn btn-primary btn-block" type="submit" value="Login" >Sign in</button>
         </form:form>
    </div>
    </body>
</html>