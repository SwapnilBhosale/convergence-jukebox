<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta name="description" content="">
    	<meta name="author" content="">
  		<script src="javascripts/jquery.min.js"></script>
  		<script src="javascripts/home.js"></script>
  		<script src="javascripts/bootstrap.min.js"></script>
    	<link href="css/bootstrap.min.css" rel="stylesheet">
    	<link href="css/home.css" rel="stylesheet">
    	<link href="css/home_slide.css" rel="stylesheet">
		<title>Booking Home</title>
	</head>
	<body>
		<ul class="home-slideshow">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
        <div>
        	<c:if test="${errorName ne '' || errorName ne null}">	
        		<c:out value="${errorName }"></c:out>
        	</c:if>
        </div>
		<div align="center" class="container-fluid container-full">
			<div class="row">
				<div class="col-md-1 col-xs-1 col-sm-1"></div>
				<div class="col-md-10 col-xs-10 col-sm-10">
				<nav class="navbar navbar-inverse">
    				<a class="navbar-brand" href="#">Jukebox</a>
     				<ul class="nav navbar-nav navbar-right pull-right">
      					<li class="dropdown">
        					<a class="dropdown-toggle" data-toggle="dropdown" href="#">${userInfo.fName} ${userInfo.lName}
        					<span class="caret"></span></a>
        						<ul class="dropdown-menu top-dropdown">
          							<li><a class="navbar-link" href="logout.html">
          							<span class="glyphicon glyphicon-log-out"></span>Log out</a></li>
						        </ul>
      					</li>
      		      		
    				</ul>
				</nav>
				</div>
				<div class="col-md-1 col-xs-1 col-sm-1"></div>
			</div>
			<div class="row">
			<div class="col-md-3 col-xs-3 col-sm-3">
			<nav class="navbar navbar-inverse" role="navigation">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
                       Welcome
                    </a>
                </li>
                <li>
                    <a href="#">Jukebox Home</a>
                </li>
                <li>
                    <a data-toggle="modal" data-target="#under-construction">About</a>
                </li>
                <li>
                    <a data-toggle="modal" data-target="#under-construction">Popular Songs</a>
                </li>
                <li>
                    <a data-toggle="modal" data-target="#under-construction">Team</a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Intranet Links <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="http://intranet.gslab.com">Intranet Home</a></li>
                    <li><a href="http://lunch.gslab.com">Lunch/Snacks Booking</a></li>
                    <li><a href="https://ess.gslab.com/">ESS - GS Lab</a></li>
                    <li><a href="http://intranet.gslab.com/employee_details/">Employee Contact Details</a></li>
                    <li><a href="http://intranet.gslab.com/seat_locator/index.html">Seat Locator</a></li>
                  </ul>
                </li>
                <li>
                    <a data-toggle="modal" data-target="#under-construction">Upload Songs</a>
                </li>
                <li>
                    <a data-toggle="modal" data-target="#under-construction">Feedback</a>
                </li>
                <li>
                    <a data-toggle="modal" data-target="#under-construction">Help</a>
                </li>
            </ul>
        	</nav>
			</div>
			<div class="modal fade" id="under-construction" role="dialog">
    			<div class="modal-dialog modal-sm">
      				<div class="modal-content">
        				<div class="modal-header">
       					   <button type="button" class="close" data-dismiss="modal">&times;</button>
				          <h4 class="modal-title">Under Construction!</h4>
 				       </div>
 				       <div class="modal-body">
 					         <p>This feature will be available soon!</p>
 				       </div>
        				<div class="modal-footer">
          					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        				</div>
      				</div>
    			</div>
  			</div>
			<div class="col-md-6 col-xs-6 col-sm-6">   
			<div class="booking-wrapper">
			    <div class="modal fade" id="modal-container" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
								x
							 </button>
							<h4 class="modal-title" id="myModalLabel">
									Select Song
							</h4>
						</div>
      					<div class="modal-body">
	      					<form id="custom-search-form" class="form-search form-horizontal clearfix">
				                <div class="input-append ">
								<span class="glyphicon glyphicon-search pull-right"></span>
				                <input class="search-query mac-style pull-right" placeholder="Search" type="text">
				                </div>
				            </form>
				            <form class="form-booking" role="form" method="post" action="book_slot.html">
	      						<div class="displayTableFrame">
		      						<table name="song-list" class="table table-hover" border="1" cellpadding="5" style="width: 100%">
							            <tbody>
							            <c:forEach var="song" items="${songList}" end="9">
						                	<tr>
												<td class="song_item" song_id="${song.songId}"><c:out value="${song.name}" /></td>
				                    		</tr>
							            </c:forEach>
			            				</tbody>
		        					</table>
	        					</div>
								<input type="hidden" name="Sel_user" id="Sel_user" value="${userInfo.username}">
	      						<input type="hidden" name="Sel_slot" id="Sel_slot">
	      						<input type="hidden" name="Sel_session" id="Sel_session">
	      						<input type="hidden" name="Sel_song" id="Sel_song">
								<div class="modal-footer"> 
									<button id="reset" type="button" class="btn btn-danger" data-dismiss="modal">
										Cancel
									</button> 
									<button class="btn btn-primary" type="submit" value="Book_slot">
										Book
									</button>
								</div>
							</form>
						</div>
					</div>
					</div>
					</div>
					
  				<ul class="nav nav-tabs" id="myTab">
    				<li class="active"><a data-toggle="tab" href="#home">Lunch</a></li>
    				<li><a data-toggle="tab" href="#menu1">Snacks</a></li>
  				</ul>
  				<div class="tab-content">
    				<div id="home" class="tab-pane fade in active">
         				<h3>All Slots for Lunchtime</h3>
      <table class="table table-hover" border="1" cellpadding="5">
            <thead>
            <tr>
                <th class="slot_col table-colnm">Slot Time</th>
                <th class="table-colnm">Song Title</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="user" items="${lunchSongsList}">
                <c:choose>
                <c:when test="${user.booked == '1'}">
                	<tr class="booked">
					<td><c:out value="${user.slotName}" /></td>
                    <td><c:out value="${user.songBean.name}" /></td>
                    </tr>
                </c:when>
                <c:otherwise>
                	<tr class="free">
                	<td ><c:out value="${user.slotName}" /></td>
                    <td class="free-info" data-toggle="modal" data-target="#modal-container" session="1" slot="${user.slotNo}">Free Slot</td>
                    </tr>
                </c:otherwise>
                </c:choose>
            </c:forEach>
            </tbody>
        </table>     
    
        			</div>
    				<div id="menu1" class="tab-pane fade">
    				      					<h3>All Slots for Snackstime</h3>
	  					<table class="table table-hover" border="1" cellpadding="5">
            				<thead>
            					<tr>
                					<th class="slot_col table-colnm">Slot Time</th>
                					<th class="table-colnm">Song Title</th>
            					</tr>
            				</thead>
            				<tbody>
            					<c:forEach var="user" items="${snacksSongsList}">
                					<c:choose>
                <c:when test="${user.booked == '1'}">
                	<tr class="booked">
					<td><c:out value="${user.slotName}" /></td>
                    <td><c:out value="${user.songBean.name}" /></td>
                    </tr>
                </c:when>
                <c:otherwise>
                	<tr class="free">
                	<td ><c:out value="${user.slotName}" /></td>
                    <td class="free-info" data-toggle="modal" data-target="#modal-container" session="2" slot="${user.slotNo}">Free Slot</td>
                    </tr>
                </c:otherwise>
                </c:choose>
            </c:forEach>
            </tbody>
        </table>
    </div>
  </div>
  </div>
  </div>
  <div class="col-md-3 col-xs-3 col-sm-3"></div>
  </div>
</div>
</body>
</html>