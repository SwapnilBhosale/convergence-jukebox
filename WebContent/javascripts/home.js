 $(document).ready(function(){
	var activeTab = localStorage.getItem('activeTab');
	var mostpopularhtml = $(".displayTableFrame").html();
    if(activeTab){
    $('#myTab a[href="' + activeTab + '"]').tab('show');
    }
    
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    
    $(".free-info").click(function(){
    	$("#Sel_slot").val($(this).attr('slot'));
    	$("#Sel_session").val($(this).attr('session'));
    });
    $(".song_item").click(function(){
    	$('.song_item-active').removeClass('song_item-active').addClass('song_item');
        $(this).toggleClass('song_item-active');
     	$("#Sel_song").val(+$(this).attr('song_id'));
    });
    
    $('#modal-container').on('hidden.bs.modal', function () {
    	$("#Sel_slot").val("");
    	$("#Sel_session").val("");
    	$("#Sel_song").val("");
    	$('.song_item-active').removeClass('song_item-active').addClass('song_item');
    	$(".displayTableFrame").html(mostpopularhtml) ;
    });
    $('#reset').click(function(){
    	$("#Sel_slot").val("");
    	$("#Sel_session").val("");
    	$("#Sel_song").val("");
    	$('.song_item-active').removeClass('song_item-active').addClass('song_item');
    });
    $("#custom-search-form .search-query").keypress(function (e) {
    	if (e.keyCode == 13) {
    		var searchkey = $("#custom-search-form .search-query").val();
    		console.log(searchkey);
    		$.ajax({
                type:"GET",
                url: "search.html?q=" + searchkey,
                dataType: 'html',
                cache: false,
              }).done(function (data) {
              	$(".displayTableFrame").html("<h4>"+ data + "</h4>") ;
            });
    		  console.log("done till this step")
    		return false;
	    }
	 });
}); 

